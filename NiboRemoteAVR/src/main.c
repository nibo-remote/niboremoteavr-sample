/*  BSD-License:

 Copyright (c) 2013 by Tobias Ilte, TH-Wildau, Germany
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name nicai-systems nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 @file		main.c
 @brief		Sample Application for NiboRemote.
 @author	Tobias Ilte

 This file defines the main-function, which contains the main-loop.
 */

#include <avr/interrupt.h>
#include <nibo/niboconfig.h>
#include <nibo/iodefs_nibo2.h>
#include <nibo/nds3.h>
#include <nibo/i2cmaster.h>
#include <nibo/spi.h>
#include <nibo/leds.h>
#include <nibo/copro.h>
#include <nibo/floor.h>
#include <nibo/pwm.h>
#include <nibo/display.h>
#include <nibo/sound.h>
#include <nibo/gfx.h>
#include <nibo/bot.h>

#include <nibo/uart0.h>
#include <nibo/uart1.h>

//NiboRemote
#include <nsp_parser.h>
#include <nsp_connection.h>

int main() {
	//===initialization===
	sei();
	bot_init();  	// Initialize the nibo, reset external controller etc...
	i2c_init();
	nds3_move(90);	// NDS3 in central position
	spi_init();
	display_init();
	sound_init();
	gfx_init();
	floor_init();
	gfx_fill(0);
	leds_init();
	pwm_init();
	nsp_init(1); // initialize Uart1
	copro_ir_startMeasure();
	//======================

	// main-loop of the robot
	while (1) {
		// NiboRemote-communication for Uart1 (checks for new messages and answers them)
		nsp_handle_data(1);

		// Do all the other stuff, the robot should do...
	}
	return 0;
}
